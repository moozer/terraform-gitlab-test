terraform-gitlab-test
====================================

We want to use gitlab as backend for our terraform state, and to use the CI.


References
-----------------------------

* [gitlab docs](https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html)
